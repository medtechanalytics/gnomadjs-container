# Docker Image for gnomAD.js

This project creates a Docker image for the [gnomAD browser](https://github.com/macarthur-lab/gnomadjs).

### Run the pre-built image

If you prefer to use the pre-built container

```bash
docker run -p 127.0.0.1:8080:8008 registry.gitlab.com/medtechanalytics/gnomadjs-container
```

### Test

After you see the following message

```bash
[wdm]: Compiled successfully.
```

open your browser and go to [http://localhost:8080](http://localhost:8080).


### Build the image, if you prefer

Build and run the docker image from the project's `Dockerfile`:

```bash
docker build -t gnomad/source .
docker run -p 127.0.0.1:8080:8008  gnomad/source
```
