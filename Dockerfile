FROM node:10.18

RUN apt-get update && apt install -y git nodejs

RUN mkdir /var/app
RUN cd /var/app && git clone --recursive --shallow-since=2019-12-01 https://github.com/macarthur-lab/gnomadjs.git
RUN cd /var/app/gnomadjs && git checkout 70b86b3a387a7a272456c1c66f54b0f3aa4b89a9
RUN cd /var/app/gnomadjs && yarn

WORKDIR /var/app/gnomadjs/projects/gnomad
EXPOSE 8008
CMD export LOGNAME=vagrant && yarn start
